import { CourtStartComponent } from './courts/court-start/court-start.component';
import { CourtsComponent } from './courts/courts.component';
import { CourtComponent } from './courts/court/court.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'courts', pathMatch: 'full' },
  { path: 'courts', component: CourtsComponent, children: [
    { path: '', component: CourtStartComponent },
    { path: ':id', component: CourtComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
