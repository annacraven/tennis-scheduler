import { City } from './../../../models/city.model';
import { CitiesService } from './../../../services/cities.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selected',
  templateUrl: './selected.component.html',
  styleUrls: ['./selected.component.css']
})
export class SelectedComponent implements OnInit {
  selectedCities = {};
  all = true; // false if not all cities are selected

  constructor(private citiesService: CitiesService) { }

  ngOnInit(): void {
    this.selectedCities = this.citiesService.selectedCities;
  }

  onSelect() {
    this.all = this.allSelected();
    this.citiesService.updateSelected(this.selectedCities);
  }

  onSelectAll() {
    for (let [key, value] of Object.entries(this.selectedCities)) {
      this.selectedCities[key] = true;
    }
    this.citiesService.updateSelected(this.selectedCities);
  }

  private allSelected() {
    for (let [key, value] of Object.entries(this.selectedCities)) {
      if (!this.selectedCities[key]) {
        return false;
      }
    }
    return true;
  }

  getCityName(id: number): string {
    return this.citiesService.getCity(id).name;
  }

}
