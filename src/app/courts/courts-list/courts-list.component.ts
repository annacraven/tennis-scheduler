import { AvailabilitiesService } from './../../services/availabilities.service';
import { Reservation } from './../../models/reservation.model';
import { CitiesService } from './../../services/cities.service';
import { CourtsService } from '../../services/courts.service';
import { Court } from '../../models/court.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-courts-list',
  templateUrl: './courts-list.component.html',
  styleUrls: ['./courts-list.component.css']
})
export class CourtsListComponent implements OnInit, OnDestroy {
  courtsList: Court[];
  selectedSub: Subscription;
  // for each court_id, how many courts are currently available?
  availabilities: number[];

  constructor(
    private courtsService: CourtsService,
    private citiesService: CitiesService,
    private availabilitiesService: AvailabilitiesService) { }

  ngOnInit(): void {
    this.courtsList = this.courtsService.getCourtsList();
    this.availabilities = this.availabilitiesService.getAvailabilities();
    this.selectedSub = this.citiesService.selectedChanged
      .subscribe(
        () => {
          this.courtsList = this.courtsService.getCourtsList();
        }
      );
    this.availabilitiesService.availabilitiesChanged$
      .subscribe(
        () => {
          this.availabilities = this.availabilitiesService.getAvailabilities();
        }
      );
  }

  ngOnDestroy(): void {
    this.selectedSub.unsubscribe();
  }

  getAvailability(court: Court) : number {
    return this.availabilities[court.id];
  }

  getCityName(court: Court) {
    return this.citiesService.getCity(court.city_id).name;
  }

}
