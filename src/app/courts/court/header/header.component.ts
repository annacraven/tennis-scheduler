import { CourtsService } from './../../../services/courts.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Court } from './../../../models/court.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-court-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class CourtHeaderComponent implements OnInit {
  @Input() court: Court;
  // id: number;

  constructor(
    private courtsService: CourtsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // this.route.params
    //   .subscribe(
    //     (params: Params) => {
    //       this.id = +params['id'];
    //       this.court = this.courtsService.getCourt(this.id);
    //     }
    //   )
  }

}
