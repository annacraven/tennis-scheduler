import { CalendarService } from './../../services/calendar.service';
import { CourtsService } from './../../services/courts.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';


import { Court } from './../../models/court.model';

@Component({
  selector: 'app-court',
  templateUrl: './court.component.html',
  styleUrls: ['./court.component.css'],
})
export class CourtComponent implements OnInit {
  court: Court;
  id: number;

  constructor(
    private courtsService: CourtsService,
    private route: ActivatedRoute,
    private calendarService: CalendarService
  ) { }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.court = this.courtsService.getCourt(this.id);
          this.calendarService.setCourt(this.court);
        }
      )
  }

}
