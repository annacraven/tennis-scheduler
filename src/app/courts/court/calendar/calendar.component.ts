import { DateTimeService } from './../../../services/date-time.service';
import { ReserveService } from './../../../services/reserve.service';
import { Subscription } from 'rxjs';
import { CalendarService } from './../../../services/calendar.service';
import { Reservation } from './../../../models/reservation.model';
import { Court } from './../../../models/court.model';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnChanges {
  @Input() court: Court;
  highlighted = {};

  // for generating the empty calendar
  numCourts: number[] = [];
  times: string[] = []; // [8:00 AM - 8:30 PM]

  constructor(
    private calendarService: CalendarService,
    private dateTimeService: DateTimeService,
    private reserveService: ReserveService
    ) { }

  ngOnInit(): void {
    this.numCourts = Array(this.court?.total);
    this.times = this.dateTimeService.getTimes();

    this.calendarService.clearHighlight$
      .subscribe(() => {
        this.highlighted = {};
      })
  }

  ngOnChanges() {
    this.numCourts = Array(this.court?.total);
  }

  isReserved(court_number: number, time: string) {
    return this.calendarService.isReserved(court_number, time);
  }

  updateHighlight(court_number: number, time: string) {
    if (this.isReserved(court_number, time)) return;

    if (!this.highlighted[court_number]) {
      this.highlighted = {};
      this.highlighted[court_number] = {};
    }
    this.highlighted[court_number][time] = !this.highlighted[court_number][time];

    this.reserveService.setForm(this.highlighted);
  }

  isHighlighted(court_number: number, time: string) {
    const courtObj = this.highlighted[court_number];
    return courtObj? courtObj[time] : false;
  }

}
