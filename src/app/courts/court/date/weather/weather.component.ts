import { WeatherDay, NULL_WEATHER_DAY } from './../../../../models/weather-day.model';
import { DateTimeService } from './../../../../services/date-time.service';
import { CalendarService } from './../../../../services/calendar.service';
import { Weather } from './../../../../models/weather.model';
import { Court } from './../../../../models/court.model';
import { WeatherService } from './../../../../services/weather.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit, OnChanges {
  @Input() court: Court;
  day: Date;
  weather: Weather;
  weatherDay: WeatherDay = NULL_WEATHER_DAY;
  // where are we in days array? -1 if n/a
  daysIndex: number = 0;


  constructor(
    private weatherService: WeatherService,
    private calendarService: CalendarService,
    private dateTimeService: DateTimeService) { }

  ngOnInit(): void {
    this.day = this.calendarService.date;
    // when date changes, update day and this.weather
    this.calendarService.dateChanged$
      .subscribe((newDate: Date) => {
        this.day = newDate;
        this.updateDaysIndex();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    // if court changes, ask weather service for weather.
    // then update this.weather
    if (changes.court) {
      this.weatherService.queryWeather(this.court)
        .subscribe(
        (responseData: any) => {
          // TODO I wish the weather service could just return Weather
          this.weather =
            this.weatherService.responseToWeather(responseData, this.court);
        },
        (err) => { console.log(err) },
        () => {
          this.updateWeather()
        });
    }
  }

  private updateWeather() {
    if (this.daysIndex == -1 ) {
      this.weatherDay = NULL_WEATHER_DAY;
      return;
    }
    this.weatherDay = this.weather.days[this.daysIndex];
  }

  // should be called whenever this.day changes
  // look at weather's last updated, and compare it to day
  // will tell you how weather.days[0] compares to this day
  // if this.day is before days[0], it's in the past. -1; no weather for now
  // (later may implement past weather)
  // if this.day is less than a week after days[0], then we do have weather
  // otherwise, -1
  private updateDaysIndex() {
    const diff = this.dateTimeService.diffDays(this.weather.last_updated, this.day);

    if (diff < 0 || diff > 7) {
      this.daysIndex = -1;
    }
    else {
      this.daysIndex = diff;
    }
    this.updateWeather();
  }
}
