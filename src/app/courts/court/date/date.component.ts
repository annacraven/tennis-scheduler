import { Court } from './../../../models/court.model';
import { CalendarService } from './../../../services/calendar.service';
import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  @Input() court: Court; // only need this for weather. TODO possibly refactor?
  selectedDate: Date;
  @ViewChild('dateInput') dateInput: ElementRef;
  pipe = new DatePipe('en-US');

  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
    this.selectedDate = new Date();
    this.onChangeDate();
  }

  changeDate(delta: number) {
    this.selectedDate.setDate(this.selectedDate.getDate() + delta);
    this.dateInput.nativeElement.value = this.pipe.transform(this.selectedDate, 'M/d/yyyy');
    this.onChangeDate();
  }

  onChangeDate() {
    this.calendarService.setDate(this.selectedDate);
  }

}
