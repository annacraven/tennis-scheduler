import { ReserveInput, NULL_RESERVE_INPUT } from './../../../models/reserve-input.model';
import { ReserveService } from './../../../services/reserve.service';
import { CalendarService } from './../../../services/calendar.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Court } from './../../../models/court.model';
import { ReservationsService } from './../../../services/reservations.service';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.css'],
  providers: [MatSnackBar]
})
export class ReserveComponent implements OnInit {
  @Input() court: Court;
  numCourts: number[] = [];
  user: string;
  reserveInput: ReserveInput = NULL_RESERVE_INPUT;

  constructor(
    private reservationsService: ReservationsService,
    private calendarService: CalendarService,
    private reserveService: ReserveService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.numCourts = Array(this.court.total);
    this.reserveService.formChanged$
      .subscribe((reserveInput: ReserveInput) => {
        this.reserveInput = reserveInput;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.court) {
      this.numCourts = Array(this.court.total);
    }

  }

  onSubmit() {
    const res = this.reservationsService.
      addReservation(this.court, this.calendarService.date, this.reserveInput, this.user);
    if (!res) {
      this.snackBar.open('Reservation Made!', 'OK', { duration: 2000 });
      this.onClear();
    } else {
      this.snackBar.open('Failed to make reservation.', 'OK', { duration: 3000 });
    }
  }

  onClear() {
    this.reserveInput = NULL_RESERVE_INPUT;
    this.user = ''; // TODO makes name warning color
    this.calendarService.clearHighlight$.next();
  }

}
