import { CitiesService } from './cities.service';
import { Court } from './../models/court.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CourtsService {
  courtsList: Court[] = [
    new Court(
      0,
      "Westlake High School",
      0,
      5,
      { lat: 41.4609, lon: -81.9289 }
    ),
    new Court(
      1,
      "Avon High School",
      1,
      5,
      { lat: 41.4447, lon: -82.0381 }
    ),
    new Court(
      2,
      "Clague Park",
      0,
      5,
      { lat: 41.4651, lon: -81.8859 }
    ),
    new Court(
      3,
      "Bradley Road Park",
      3,
      5,
      { lat: 41.483205, lon: -81.960026 }
    ),
    new Court(
      4,
      "The Tennis Club",
      2,
      8,
      { lat: 41.4836, lon: -82.0192 }
    ),
    new Court(
      5,
      "Avon Lake High School",
      2,
      8,
      { lat: 41.5083, lon: -82.0166 }
    ),
    new Court(
      6,
      "Tri-City Park",
      0,
      4,
      { lat: 41.4509, lon: -81.8745 }
    ),
    new Court(
      7,
      "Westlake Rec Center",
      0,
      5,
      { lat: 41.4454, lon: -81.9389 }
    ),
    new Court(
      8,
      "Hyland",
      0,
      2,
      { lat: 41.471990, lon: -81.935219 }
    )
  ]

  getCourtsList() {
    return this.courtsList.filter((court: Court) => {
      return this.citiesService.getSelectedCities()[court.city_id];
    }).sort(Court.compare);

  }

  getCourt(id: number) {
    return this.courtsList[id];
  }

  constructor(private citiesService: CitiesService) { }
}
