import { CourtsService } from './courts.service';
import { Reservation } from './../models/reservation.model';
import { Court } from './../models/court.model';
import { DateTimeService } from './date-time.service';
import { ReservationsService } from './reservations.service';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AvailabilitiesService {
  availabilities: number[] = [];
  // decided not to have subject emit the change, because if I add the
  // change when 30 minute chunk changes, then all the availabilities will change
  // and now I can just use this same subject instead of another one
  availabilitiesChanged$ = new Subject()

  constructor(
    private courtsService: CourtsService,
    private reservationsService: ReservationsService,
    private dateTimeService: DateTimeService
  ) {
    this.initAvailabilities();
    this.reservationsService.reservationAdded$
      .subscribe((r: Reservation) => {
        this.updateAvailabilities(r);
      })
  }

  getAvailability(court: Court): number {
    return this.availabilities[court.id];
  }

  getAvailabilities() {
    return this.availabilities.slice();
  }

  private updateAvailabilities(r: Reservation) {
    if (this.dateTimeService.between(r, new Date())) {
      this.availabilities[r.court_id]--;
      this.availabilitiesChanged$.next();
    }
  }

  private initAvailabilities() {
    let now = new Date();
    for (let court of this.courtsService.courtsList) {
      // when I switch to db, = this.getAvailability(court)
      let reservations = this.reservationsService.getReservations(court, now);
      let availability = court.total;
      for (let r of reservations) {
        if (this.dateTimeService.between(r, now)) {
          availability--;
        }
      }
      this.availabilities[court.id] = availability;
    }
  }
}
