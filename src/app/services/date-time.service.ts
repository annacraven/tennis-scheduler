import { Reservation } from './../models/reservation.model';
import { Injectable } from '@angular/core';
import { formatDate } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class DateTimeService {
  times: string[] = [];
  finalTime: string; // at the moment it's 8:30 PM

  constructor() {
    this.setTimes();
    this.finalTime = this.times.slice(-1)[0];
  }

  // return the next time in the time array
  nextTime(time: string) : string {
    let last = this.times.slice(-1)[0];
    if (time == this.finalTime) {
      // TODO fix this so it isn't hard coded
      return '9:00 PM'
    }
    return this.times[this.times.indexOf(time) + 1];
  }

  getTimeByIndex(index: number) : string {
    return this.times[index];
  }

  getTimeIndex(time: string) : number {
    // for reservations that end at the bottom of the calendar
    // end is 30 minutes after the final time
    if (time == this.nextTime(this.finalTime)) {
      return this.times.length
    }
    return this.times.indexOf(time);
  }

  // 8:00 PM to 2000
  // doesn't work with 12am because I shouldn't need that time.
  timeToInt(time: string) : number {
    let int = +time.replace(/ .M/, '').replace(':', '');
    if (time.match('PM') && int <= 1159) {
      int += 1200;
    }
    return int;
  }

  // @return 9:00 AM
  dateToTime(date: Date): string {
    return formatDate(date, 'h:mm a', 'en-US')
  }

  getTimes() : string[] {
    return this.times.slice();
  }

  between(r: Reservation, time: Date) : boolean {
    return (time >= r.start) && (time <= r.end);
  }

  // takes the input from reserve form and date component
  dateAddTime(day: Date, time: string) : Date {
    return new Date(day.toDateString() + ' ' + time)
  }

  // returns true if same day
  compareDay(a: Date, b: Date) {
    return (a.getDate() == b.getDate()) &&
      (a.getMonth() == b.getMonth()) &&
      (a.getFullYear() == b.getFullYear());

  }

  diffDays(a: Date, b: Date): number {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  // generate array of time strings, [8:00AM - 8:30PM]
  private setTimes() : void {
    let x = 30; //minutes interval
    let tt = 60*8; // start time
    let ap = ['AM', 'PM'];

    //loop to increment the time and push results in array
    for (let i=0;tt<21*60; i++) {
      let hh = Math.floor(tt/60); // getting hours of day in 0-24 format
      let mm = (tt%60); // getting minutes of the hour in 0-55 format
      this.times[i] = ("" + ((hh==12)?12:hh%12)).slice(-2) + ':' + ("0" + mm).slice(-2) + ' ' + ap[Math.floor(hh/12)];
      tt = tt + x;
    }
  }
}
