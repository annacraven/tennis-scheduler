import { DateTimeService } from './date-time.service';
import { Subject } from 'rxjs';
import { ReserveInput } from './../models/reserve-input.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReserveService {
  formChanged$ = new Subject<ReserveInput>();

  constructor(private dateTimeService: DateTimeService) { }

  setForm(highlighted: {}) {
    this.formChanged$.next(this.parseHighlight(highlighted));
  }

  private parseHighlight(highlighted: {}) : ReserveInput {
    let court_number = 0;
    let start = '';
    let end = '';
    for (const key in highlighted) {
      court_number = +key;
      let min = this.dateTimeService.timeToInt('11:59 PM');
      let max = this.dateTimeService.timeToInt('1:00 AM');
      start = '';
      end = ''
      for (const time in highlighted[key]) {
        if (!highlighted[key][time]) continue;
        // if I can assume there's no gaps, could just find min and max time
        // how can I force no gaps?
        const int_time = this.dateTimeService.timeToInt(time);
        if (int_time >= max) {
          end = this.dateTimeService.nextTime(time);
          max = this.dateTimeService.timeToInt(end);
        }
        if (int_time <= min) {
          start = time;
          min = int_time;
        }
      }
      break;
    }
    let valid = this.validateInput(court_number, start, end, highlighted);
    return { court_number: court_number, start: start, end: end, valid: valid };
  }

  private validateInput(court_number: number, start: string, end: string, highlighted: {})
    : boolean
  {
    // check for gaps
    // TODO make iterator for times
    let i = this.dateTimeService.getTimeIndex(start);
    let time = this.dateTimeService.getTimeByIndex(i);
    while (time != end) {
      if (!highlighted[court_number][time]) {
        return false;
      }
      time = this.dateTimeService.nextTime(time);
    }
    return true;
  }

}
