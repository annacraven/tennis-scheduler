import { DateTimeService } from './date-time.service';
import { ReservationsService } from './reservations.service';
import { Court } from './../models/court.model';
import { Subject } from 'rxjs';
import { Reservation } from './../models/reservation.model';
import { Injectable } from '@angular/core';

// TODO provide somewhere else?
@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  date: Date = new Date();
  clearHighlight$ = new Subject();
  dateChanged$ = new Subject<Date>();

  court: Court;
  // object to tell whether a certain in the calendar should be filled in
  // filledIn[court_number][time] = reservation.id
  filledIn = {};

  constructor(
    private reservationsService: ReservationsService,
    private dateTimeService: DateTimeService) {
    this.reservationsService.reservationAdded$.
      subscribe((r: Reservation) => {
        this.addToFilledIn(r);
      })
  }

  setCourt(court: Court) {
    this.court = court;
    this.setFilledIn();
  }

  setDate(date: Date) {
    this.date = date;
    this.dateChanged$.next(date);
    this.setFilledIn();
  }

  // returns who made the reservation
  // will change when I add authentication
  isReserved(court_number: number, time: string) : string {
    const r_id = this.getFilledIn(court_number, time);
    if (r_id != undefined) {
      const r = this.reservationsService.getReservation(r_id);
      return r.user;
    }
    return '';
  }

  // get reservations for this court and date
  // translate to filledIn object
  private setFilledIn() {
    this.filledIn = {};
    let reservations: Reservation[] =
      this.reservationsService.getReservations(this.court, this.date);
    for (let r of reservations) {
      this.addToFilledIn(r);
    }
  }

  // @return reservation id
  private getFilledIn(court_number: number, time: string) : number {
    if (court_number in this.filledIn) {
      return this.filledIn[court_number][time];
    }
    else {
      return undefined;
    }
  }

  private addToFilledIn(r: Reservation) {
    // TODO make a times iterator in datetimes service
    // get indices for iterating though times array in datetime service
    const start_index = this.dateTimeService.getTimeIndex(
      this.dateTimeService.dateToTime(r.start));
    const end_index = this.dateTimeService.getTimeIndex(
      this.dateTimeService.dateToTime(r.end));
    for (let i = start_index; i < end_index; i++) {
      // if court_number is not already a key, init to {}
      (r.court_number in this.filledIn) || (this.filledIn[r.court_number] = {});

      this.filledIn[r.court_number]
        [this.dateTimeService.getTimeByIndex(i)] = r.id;
    }
  }

}
