import { WeatherDay } from './../models/weather-day.model';
import { Weather } from './../models/weather.model';
import { Court } from './../models/court.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// TODO where to put this?
const API_KEY = '002afa32f9884ce534f22c247fe3b996';
const BASE_URL = 'https://api.openweathermap.org/data/2.5/onecall?';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  queryWeather(court: Court): Observable<any> {
    let url =
      `${BASE_URL}lat=${court.location.lat}&lon=${court.location.lon}&units=imperial&exclude=hourly,minutely&appid=${API_KEY}`;
    return this.http.get(url);
  }

  // takes response from weather api and parses it to create new Weather object
  responseToWeather(responseData, court: Court): Weather {
    let days = [];
    days[0] = this.parseData(responseData['current']);
    for (let i = 1; i < responseData['daily'].length; i++) {
      days[i] = this.parseData(responseData['daily'][i]);
    }
    return new Weather(court.id, days);
  }

  // @return [WeatherDay] the weather info we care about
  private parseData(data:
    { weather: {}, wind_speed: number, temp: {day: number} | number}
  ): WeatherDay {
    const icon = data['weather'][0]['icon'];
    const windSpeed = data['wind_speed'];
    const wind = this.setWind(windSpeed);
    const temp = data.temp instanceof Object? data.temp.day : data.temp;

    return {icon: icon, windSpeed: wind, temp: temp};
  }

  private setWind(windSpeed: number) : string {
    if (windSpeed <= 5) {
      return 'low';
    }
    else if (windSpeed <= 15 ) {
      return 'medium';
    }
    else if (windSpeed <= 25) {
      return 'high';
    }
    else {
      return 'very high';
    }
  }
}
