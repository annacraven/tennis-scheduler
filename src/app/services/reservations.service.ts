import { ReserveInput } from './../models/reserve-input.model';
import { DateTimeService } from './date-time.service';
import { Subject } from 'rxjs';
import { Court } from './../models/court.model';
import { Reservation } from './../models/reservation.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {
  // to be removed
  fake_start = new Date();
  fake_end = new Date();
  //
  reservationAdded$ = new Subject<Reservation>();

  constructor(private dateTimeService: DateTimeService) {
    this.fake_start.setHours(10,0,0,0);
    this.fake_end.setHours(18,0,0,0);
  }

  reservations: Array<Reservation> = [
    new Reservation(0, 0, 1, this.fake_start, this.fake_end, "Steve"),
    new Reservation(1, 2, 2, this.fake_start, this.fake_end, "Don"),
    new Reservation(2, 2, 3, this.fake_start, this.fake_end, "Demi"),
    new Reservation(3, 2, 1, this.fake_start, this.fake_end, "Rachel"),
    new Reservation(4, 2, 4, this.fake_start, this.fake_end, "Zach"),
    new Reservation(5, 2, 5, this.fake_start, this.fake_end, "Ladies Doubles"),
    new Reservation(6, 8, 1, this.fake_start, this.fake_end, "Jerry"),
    new Reservation(7, 8, 2, this.fake_start, this.fake_end, "Jason"),

  ];

  getReservations(court: Court, day: Date) {
    return this.reservations.filter((r: Reservation) => {
      return (r.court_id == court.id) &&
        this.dateTimeService.compareDay(day, r.start);
    })
  }

  getReservation(id: number) {
    return this.reservations[id];
  }

  addReservation(court: Court, day: Date, reserveInput: ReserveInput, user: string) {
    if (!court || !reserveInput.court_number || !reserveInput.start || !reserveInput.end || !user) {
      return 1;
    }
    const startDate = this.dateTimeService.dateAddTime(day, reserveInput.start);
    const endDate = this.dateTimeService.dateAddTime(day, reserveInput.end);
    const r = new Reservation(
      this.reservations.length,
      court.id, reserveInput.court_number, startDate, endDate, user
    );
    this.reservations.push(r);
    this.reservationAdded$.next(r);
    return 0;
  }
}
