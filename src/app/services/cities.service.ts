import { City } from './../models/city.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  cities: City[] = [
    new City(
      0,
      "Westlake",
    ),
    new City(
      1,
      "Avon",
    ),
    new City(
      2,
      "Avon Lake",
    ),
    new City(
      3,
      "Bay Village",
    ),
  ]

  selectedChanged = new Subject<{}>();
  // city_id : true
  selectedCities = {}

  getCities() {
    return this.cities.slice();
  }

  getCity(id: number) {
    return this.cities[id];
  }

  getSelectedCities() {
    return this.selectedCities;
  }

  updateSelected(newSelected) {
    this.selectedCities = newSelected;
    this.selectedChanged.next();
  }

  constructor() {
    for (let city of this.cities) {
      this.selectedCities[city.id] = true;
    }
  }
}
