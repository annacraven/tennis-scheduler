export interface ReserveInput {
  court_number: number;
  start: string;
  end: string;
  valid: boolean;
}

export const NULL_RESERVE_INPUT : ReserveInput = {
  court_number: null,
  start: '',
  end: '',
  valid: false
}
