import { Location } from './location.model';

export class Court {
  constructor(
    public id: number, // todo assign automatically
    public name: string,
    public city_id: number,
    public total: number,
    public location: Location
  ) {}

  static compare(a: Court, b: Court) {
    return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
  }
}
