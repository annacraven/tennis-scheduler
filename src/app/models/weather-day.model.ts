// interface for the information we want from the weather api
// represents one day of weather
export interface WeatherDay {
  icon: string;
  windSpeed: string;
  temp: number;
}

// used when day is in the past or more than a week in the future
// because api only gives us weather for the next week
export const NULL_WEATHER_DAY : WeatherDay = {
  icon: 'none',
  windSpeed: '',
  temp: NaN,
}
