import { Court } from './court.model';

export class Reservation {
  constructor(
    public id: number, // todo assign automatically
    public court_id: number, // should it be court?
    public court_number: number,
    public start: Date,
    public end: Date,
    public user: string,
  ) {}
}
