import { WeatherDay } from './weather-day.model';

export class Weather {
  constructor(
    public court_id: number,
    public days: WeatherDay[],
    public last_updated = new Date(),
  ) {}
}
