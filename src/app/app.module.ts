import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppMaterialModule } from './app-material/app-material.module';
import { HttpClientModule }    from '@angular/common/http';



import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CourtsListComponent } from './courts/courts-list/courts-list.component';
import { CourtComponent } from './courts/court/court.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { CourtsComponent } from './courts/courts.component';
import { SelectedComponent } from './courts/courts-list/selected/selected.component';
import { FooterComponent } from './footer/footer.component';
import { CourtHeaderComponent } from './courts/court/header/header.component';
import { DateComponent } from './courts/court/date/date.component';
import { CalendarComponent } from './courts/court/calendar/calendar.component';
import { ReserveComponent } from './courts/court/reserve/reserve.component';
import { WeatherComponent } from './courts/court/date/weather/weather.component';
import { CourtStartComponent } from './courts/court-start/court-start.component';

@NgModule({
  declarations: [
    AppComponent,
    CourtsListComponent,
    CourtComponent,
    HeaderComponent,
    CourtsComponent,
    SelectedComponent,
    FooterComponent,
    CourtHeaderComponent,
    DateComponent,
    CalendarComponent,
    ReserveComponent,
    WeatherComponent,
    CourtStartComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  // providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
